# RegovTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project Structure 

I've used Bootstrap 4 (https://getbootstrap.com/) for layout and UI components. The code is self-explanatory and straightforward. However I'd bring a few things to your attention. 

The `models` folder contains classes for defining the domain model, these are the data types returned by the RESTCountries API. 

When the app loads up, it fetches the list of all countries. The autocomplete input then uses this local data to display results as you type ahead. Once you select a country, more information is displayed such as the name, population, flag, etc. 

Obviously, this is just for demonstration purposes and we'd do things differently in a production environment. I'd use a ready-made component for the typeahead component. But if we wanted to have our own implementation, it should be developed as a re-usable component. 

Also, this app assumes the data is available locally. If we wanted to fetch remote data as we type, we'd need RxJs subjects and observables to debounce the input (say by 300ms) and work with streams instead.

I hope this finds you well, let me know if you had any questions.