import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Country } from './models/country';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  countries: Country[] = [];
  searchText: string;
  isLoading: boolean;

  results: Country[] = [];
  selectedCountry: Country;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.loadCountries();
  }

  loadCountries() {
    const url = "https://restcountries.eu/rest/v2/all";
    this.isLoading = true;

    this.http.get(url).subscribe((data: Country[]) => {
      this.countries = data;
      console.log(data);
    }).add(() => {
      this.isLoading = false;
    });
  }

  searchCountries(term: string) {
    this.results = this.countries.filter(value => {
      return value.name.toLowerCase().includes(term.toLowerCase());
    });
  }

  onSelectCountry(country: Country) {
    this.selectedCountry = country;
    this.results = [];
  }
}
